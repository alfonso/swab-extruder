# Swab Extruder

In order generate mass production of swabs, this attempt try to avoid complex shapes only possible to made with a 3d printer. Besides, its mass production forces to have a farm of printers. An extrusion is proposed to generate section constant swabs, trying to fulfill all requirements of **collection sufficiency** and **flexibiity requirements**.

## status
This project has been shelved as other swab efforts seem more promising and are further along.

## Geometry proposal

Based on the shapes that plastic swabs have, I want to try the strategy of generate thin walls of plastic instead of replicate hairs. Those thin walls could have more capacity of holds liquids samples as well as more area to viruses get attached to them. As they are so thin, they will be soft enough to flex as needed.

![section](images/section.png)

The section to extrude is composed by a central circular section, in this case of 2.3mm that provides the stiffness needed. From this inner circle converges 8 legs getting thinner along the radial direction.

![section](images/extrusion.png)

In order to achieve the stiffnes needed in the main beam and the softness needed in the exterior hairs, iterations with geometry and material proposed can be done.

## Materials

Looking for [medical-grade plastics](https://www.raumedic.com/medical-technology/medical-grade-plastics) has been a tricky thing. Not every approval plastic has the same range and looking for which material is accepted to be inserted in mouth and nose has been a rabbit hole. The most successful output Ive got here has been research which material uses a conventional medical use NP swab. Most of them are Nylon, Polyester (PET). If someone can help me here it would be nice to have some orientation.

The most interesting one would be the one with the lowing melting temperature, at least to start with the mock-up.

## Machine

Based on any conventional plastic extruder machine

![machine](images/machine.png)

It is wanted to change:

  1. Modify the extrusion part.

This piece can be done with the EDM. The length of it can be iterated to ensure that the attempt shape has enough transition length to adapt to the final complex cross section.  

![transition](images/transition.png)




![machine](images/opaque.png)

![machine](images/transparent.png)
![machine](images/detail.png)

It will be attached to a conventional screw feeder.

![feeder](images/feeder.png)

To find this item from a provider inside the US is being as well a tricky part.

The design can adapt in function of the available outer diameter part. The proposed design is 10mm.

The feed shaft would be attached to a stepper.

I think the most ideal position of this extrusion to not be affected by gravity is to put it vertically. An adaptor to feed pellet is also needed.

![machine](images/machineee.png)

The intermediate box represent the space to install the shaft adaptor between the motor and the feeder and the possible leakage system.

A closed loop heating system is needed in the exterior of the shaft, the green element.

## Parts

TBD, have a call with Sam asking about his zipping extruder.

## Test

A test die was wire-EDMed out of 12 mm aluminum plate using 150 um brass wire:

![edm_vid](images/edm.mp4)

![testdie](images/testdie.jpg)

The die was clamped to the machine using a crossbar:

![testdie-clamped](images/testdie-clamped.jpg)

This did not work well as polypropylene leaked out between the die and the injection molding machine. A second test die was fabricated to fit without clamps:

![testdie-mounted](images/testdie-mounted.jpg)

This version did not leak, but the extrusion profile did not work well; the center diameter was too large, resulting in a round extrusion with minimal fin features. After a few seconds the die also cooled enough to freeze. Further attempts were abandoned as other swab-making efforts pulled ahead.
